﻿using System.Collections.Generic;
using EmailBroadcastNeedlessDesign.MessageReceiver;

namespace EmailBroadcastNeedlessDesign.Parser
{
    public interface IReceiverDataParser<out T> where T: class,IMessageReceiver
    {
        IEnumerable<T> ParseData();
    }
}
