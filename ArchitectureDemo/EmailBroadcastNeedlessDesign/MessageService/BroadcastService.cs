﻿using System;
using EmailBroadcastNeedlessDesign.MessageReceiver;
using EmailBroadcastNeedlessDesign.Notifier;
using EmailBroadcastNeedlessDesign.Parser;

namespace EmailBroadcastNeedlessDesign.MessageService
{
    public class BroadcastService : IMessagingService
    {
        private readonly INotifier _notifier;
        private readonly IReceiverDataParser<IMessageReceiver> _messageReceiverDataParser;

        public BroadcastService(IReceiverDataParser<IMessageReceiver> messageReceiverDataParser, INotifier notifier)
        {
            _messageReceiverDataParser = messageReceiverDataParser;
            _notifier = notifier;
        }

        public void SendMessages()
        {
            var messageReceivers = _messageReceiverDataParser.ParseData();
            foreach (var messageReceiver in messageReceivers)
            {
                try
                {
                    _notifier.SendMessage(messageReceiver);
                }
                catch (Exception ex)
                {
                    //depends on the use case: you either want to continue for remaining or just stop sending messages
                    //most likely you would want to continue so log and move on
                }
            }
        }
    }
}