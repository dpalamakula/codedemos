﻿using System;
using EmailBroadcastNeedlessDesign.MessageReceiver;
using EmailBroadcastNeedlessDesign.Notifier;
using EmailBroadcastNeedlessDesign.Parser;

namespace EmailBroadcastNeedlessDesign.MessageService
{
    public class BroadcastBatchService:IMessagingService
    {
        private readonly INotifier _notifier;
        private readonly IReceiverDataParser<IMessageReceiver> _messageReceiverDataParser;

        public BroadcastBatchService(IReceiverDataParser<IMessageReceiver> messageReceiverDataParser, INotifier notifier)
        {
            _messageReceiverDataParser = messageReceiverDataParser;
            _notifier = notifier;
        }

        
        public void SendMessages()
        {
            //get batch and send notifications
            throw new NotImplementedException();
        }
    }
}
