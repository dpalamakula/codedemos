﻿namespace EmailBroadcastNeedlessDesign.MessageReceiver
{
    public enum CommunicationType
    {
        Email,
        SMS, 
        Twitter,
        None
    }

    public interface IMessageReceiver
    {
        string FirstName { get; }
        string LastName { get; }
        CommunicationType CommunicationType { get; }
        string GetCommunicationAddress();
    }
}