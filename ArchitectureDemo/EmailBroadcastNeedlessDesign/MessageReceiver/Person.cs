﻿using System;

namespace EmailBroadcastNeedlessDesign.MessageReceiver
{
    public class Person : IMessageReceiver
    {
        private readonly string _emailAddress;
        private string _firstName;
        private string _lastName;
        private readonly string _mobileNumber;
        private readonly string _twitterId;

        public Person(string firstName, string lastName, string emailAddress, string twitterId, string mobileNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            _emailAddress = emailAddress;
            _twitterId = twitterId;
            _mobileNumber = mobileNumber;
            InitializeCommunicationType();
        }

        public string EmailAddress
        {
            get { return _emailAddress; }
        }

        public string TwitterId
        {
            get { return _twitterId; }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
        }

        public string FirstName
        {
            get { return _firstName; }
            private set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("FirstName");
                }
                _firstName = value;
            }
        }

        public string LastName
        {
            get { return _lastName; }
            private set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("_lastName");
                }
                _lastName = value;
            }
        }

        public CommunicationType CommunicationType { get; private set; }

        public string GetCommunicationAddress()
        {
            switch (CommunicationType)
            {
                case CommunicationType.Email:
                    return _emailAddress;
                case CommunicationType.SMS:
                    return _mobileNumber;
                case CommunicationType.Twitter:
                    return _twitterId;
                default:
                    return string.Empty;
            }
        }

        private void InitializeCommunicationType()
        {
            if (_emailAddress != null)
            {
                CommunicationType = CommunicationType.Email;
            }
            else if (_mobileNumber != null)
            {
                CommunicationType = CommunicationType.SMS;
            }
            else if (_twitterId != null)
            {
                CommunicationType = CommunicationType.Twitter;
            }
            else
            {
                CommunicationType = CommunicationType.None;
            }
        }
    }
}