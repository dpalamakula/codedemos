﻿using System;
using EmailBroadcastNeedlessDesign.MessageReceiver;

namespace EmailBroadcastNeedlessDesign.Notifier
{
    public class NotifierFactory : INotifierFactory
    {
        public INotifier GetNotifier(CommunicationType communicationType)
        {
            INotifier notifier = null;
            switch (communicationType)
            {
                case CommunicationType.Email:
                    notifier = new EmailNotifier();
                    break;
                case CommunicationType.SMS:
                    notifier = new SmsNotifier();
                    break;
                case CommunicationType.Twitter:
                    break;
                case CommunicationType.None:
                    break;
                default:
                    throw new ArgumentOutOfRangeException("communicationType", communicationType, null);
            }

            return notifier;
        }
    }
}