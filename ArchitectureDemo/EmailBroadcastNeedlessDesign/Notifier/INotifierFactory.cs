﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmailBroadcastNeedlessDesign.MessageReceiver;

namespace EmailBroadcastNeedlessDesign.Notifier
{
    public interface INotifierFactory
    {
        INotifier GetNotifier(CommunicationType communicationType);
    }
}
