﻿using EmailBroadcastNeedlessDesign.MessageReceiver;

namespace EmailBroadcastNeedlessDesign.Notifier
{
    public interface INotifier
    {
        void SendMessage(IMessageReceiver person);
    }
}
