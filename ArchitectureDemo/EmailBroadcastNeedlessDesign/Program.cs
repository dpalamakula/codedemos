﻿using EmailBroadcastNeedlessDesign.MessageReceiver;
using EmailBroadcastNeedlessDesign.MessageService;
using EmailBroadcastNeedlessDesign.Notifier;
using EmailBroadcastNeedlessDesign.Parser;

namespace EmailBroadcastNeedlessDesign
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            INotifierFactory notifierFactory = new NotifierFactory();
            IReceiverDataParser<IMessageReceiver> personDataParser = new PersonDataFileParser();
            INotifier notifier = notifierFactory.GetNotifier(CommunicationType.Email);
            var messageService = new BroadcastService(personDataParser, notifier);
        }
    }
}