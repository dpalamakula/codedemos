﻿namespace EmailBroadcastNoDesign
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string TwitterId { get; set; }
        public string MobileNumber { get; set; }
    }
}