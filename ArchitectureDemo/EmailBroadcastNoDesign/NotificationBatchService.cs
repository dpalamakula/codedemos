﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Linq;

namespace EmailBroadcastNoDesign
{
    public class NotificationBatchService
    {
        private const int BatchSize = 100;
        private readonly bool _batchNotifications;

        public NotificationBatchService(bool batchNotifications)
        {
            this._batchNotifications = batchNotifications;
        }
        public void SendNotifications(string peopleFile)
        {
            var people = GetPeople(peopleFile);

        }

        public void NotifyAllAtOnce(List<Person> people)
        {
            foreach (var person in people)
            {
                SendEmailToPerson(person);
            }
        }

        public void NotifyInBatches(List<Person> people)
        {
            foreach (var batch in GetBatch(people))
            {
                foreach (var person in batch)
                {
                    SendEmailToPerson(person);
                }
            }
        }

        public List<Person> GetPeople(string peopleFile)
        {
            string[] peopleRecords = File.ReadAllLines(peopleFile);
            return peopleRecords.Select(personRecord => new Person()
                {
                    FirstName = peopleRecords[0],
                    LastName = peopleRecords[1],
                    EmailAddress = peopleRecords[2],
                    MobileNumber = peopleRecords[3],
                    TwitterId = peopleRecords[4]
                }).ToList();
        }

        public void SendEmailToPerson(Person person)
        {
            var mail = new MailMessage("donotreply@neogov.com", person.EmailAddress);
            var client = new SmtpClient
                {
                    Port = 25,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = "smtp.google.com"
                };
            mail.Subject = "Some random subject";
            mail.Body = "Hi Random Message";
            client.Send(mail);
        }

        public IEnumerable<List<Person>> GetBatch(List<Person> people)
        {
            int currentBatchNumber = 0;
            yield return people.Skip((++currentBatchNumber - 1) * BatchSize).Take(BatchSize).ToList();
        }
    }
}