﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Linq;

namespace EmailBroadcastNoDesign
{
    public class NotificationService
    {
        public void SendNotifications(string peopleFile)
        {
            var people = GetPeople(peopleFile);
            foreach (var person in people)
            {
                SendEmailToPerson(person);
            }
        }

        public List<Person> GetPeople(string peopleFile)
        {
            var peopleList = new List<Person>();
            string[] peopleRecords = File.ReadAllLines(peopleFile);
            foreach (var personRecord in peopleRecords)
            {
                peopleList.Add(new Person()
                    {
                        FirstName = peopleRecords[0],
                        LastName = peopleRecords[1],
                        EmailAddress = peopleRecords[2],
                        MobileNumber = peopleRecords[3],
                        TwitterId = peopleRecords[4]
                    });
            }
            return peopleList;
        }

        public void SendEmailToPerson(Person person)
        {
            var mail = new MailMessage("donotreply@neogov.com", person.EmailAddress);
            var client = new SmtpClient
                {
                    Port = 25,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Host = "smtp.google.com"
                };
            mail.Subject = "Some random subject";
            mail.Body = "Hi Random Message";
            client.Send(mail);
        }
    }
}