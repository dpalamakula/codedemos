﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShopSampleCode
{
    public enum CoffeeType
    {
        Mocha,
        Cappuccino,
        CaffeLatte,
        CaramelMacchiato
    }

    public class Barista
    {
        public Barista()
        {

        }

        public void ChargeMoneyAndDeliverCoffee(CupOfCoffee coffeeToFill, CoffeeType coffeeType, Payment payment)
        {

            ChargePayment(payment);

            //make coffee
            switch (coffeeType)
            {
                case CoffeeType.Mocha:
                    MakeMocha();
                    break;
                case CoffeeType.Cappuccino:
                    MakeCappuccino();
                    break;
                case CoffeeType.CaffeLatte:
                    MakeCaffeLatte();
                    break;
                case CoffeeType.CaramelMacchiato:
                    MakeCaramelMacchiato();
                    break;
                default:
                    break;
            }
        }

        public void ChargePayment(Payment payment)
        {
            //code to charge
        }

        public void MakeMocha()
        {
            //code to make coffee
        }

        public void MakeCappuccino()
        {
            //code to make coffee
        }

        public void MakeCaffeLatte()
        {
            //code to make coffee
        }

        public void MakeCaramelMacchiato()
        {
            //code to make coffee
        }
    }
}
