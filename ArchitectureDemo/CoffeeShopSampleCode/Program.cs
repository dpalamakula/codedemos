﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoffeeShopSampleCode
{
    class Program
    {
        static void Main(string[] args)
        {
            Barista barista = new Barista();
            CupOfCoffee cup = new CupOfCoffee();
            Payment payment = new Payment();
            barista.ChargeMoneyAndDeliverCoffee(cup, CoffeeType.CaramelMacchiato, payment);
        }
    }
}
