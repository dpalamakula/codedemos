﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EmailBroadcastNeededDesign.Notifier;
using EmailBroadcastNeededDesign.Parser;

namespace EmailBroadcastNeededDesign
{
    class Program
    {
        static void Main(string[] args)
        {
            IPersonDataParser personDataParser = new PersonDataFileParser();
            INotifier notifier = new EmailNotifier();
            IMessagingService messageService = new BroadcastService(personDataParser, notifier);
            messageService.SendMessages();
        }
    }
}
