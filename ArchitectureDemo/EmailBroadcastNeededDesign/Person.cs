﻿using System;

namespace EmailBroadcastNeededDesign
{
    public class Person
    {
        private string _firstName;
        private string _lastName;
        private string _emailAddress;
        private string _twitterId;
        private string _mobileNumber;

        public Person(string firstName, string lastName, string emailAddress, string twitterId, string mobileNumber)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            _emailAddress = emailAddress;
            _twitterId = twitterId;
            _mobileNumber = mobileNumber;
        }

        public string EmailAddress
        {
            get { return _emailAddress; }
        }

        public string TwitterId
        {
            get { return _twitterId; }
        }

        public string MobileNumber
        {
            get { return _mobileNumber; }
        }

        public string FirstName
        {
            get { return _firstName; }
            private set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("FirstName");
                }
                _firstName = value;
            }
        }

        public string LastName
        {
            get { return _lastName; }
            private set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("_lastName");
                }
                _lastName = value;
            }
        }
    }
}