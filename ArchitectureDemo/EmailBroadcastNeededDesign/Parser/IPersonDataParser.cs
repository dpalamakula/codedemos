﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailBroadcastNeededDesign.Parser
{
    public interface IPersonDataParser
    {
        IEnumerable<Person> ParseData();
    }
}
