﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailBroadcastNeededDesign.Notifier
{
    public interface INotifier
    {
        void SendMessage(Person person);
    }
}
