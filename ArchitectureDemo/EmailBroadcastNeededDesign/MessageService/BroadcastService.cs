﻿using System;
using EmailBroadcastNeededDesign.Notifier;
using EmailBroadcastNeededDesign.Parser;

namespace EmailBroadcastNeededDesign
{
    public class BroadcastService : IMessagingService
    {
        private readonly INotifier _notifier;
        private readonly IPersonDataParser _personDataParser;

        public BroadcastService(IPersonDataParser personDataParser, INotifier notifier)
        {
            _personDataParser = personDataParser;
            _notifier = notifier;
        }

        public void SendMessages()
        {
            var people = _personDataParser.ParseData();
            foreach (var person in people)
            {
                try
                {
                    _notifier.SendMessage(person);
                }
                catch (Exception ex)
                {
                    //depends on the use case: you either want to continue for remaining or just stop sending messages
                    //most likely you would want to continue so log and move on
                }
            }
        }
    }
}