﻿(function () {
    'use strict';

    angular
      .module('advisorsApp')
        .factory('authenticationService', authenticationService);

    authenticationService.$inject = ['$http', '$q', '$log', 'localStorageService'];

    function authenticationService($http, $q, $log, localStorageService) {
        var serviceBase = '/';
        var userIdentity = {
            isAuthenticated:false,
            username:''
        };

        var login = function(credentials) {

            var data = "grant_type=password&username=" + credentials.username + "&password=" + credentials.password;

            var deferred = $q.defer();

            $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                localStorageService.set('authorizationData', { token: response.access_token, username: credentials.username });

                userIdentity.isAuthenticated = true;
                userIdentity.username = credentials.username;

                deferred.resolve(response);

            }).error(function (err, status) {
                logout();
                deferred.reject(err);
            });

            return deferred.promise;
        };

        var createAccount = function (newAccount) {
            logout();

            return $http.post(serviceBase + 'api/account/register', newAccount).then(function (response) {
                return response;
            });
        };

        var logout = function () {
            localStorageService.remove('authorizationData');

            userIdentity.isAuthenticated = false;
            userIdentity.username = '';
        };

        var fillAuthData = function() {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                userIdentity.isAuthenticated = true;
                userIdentity.username = authData.userName;
            }
        };  

        return {
            login: login,
            createAccount: createAccount,
            logout: logout,
            userIdentity: userIdentity,
            fillAuthData: fillAuthData
        };
    };
})();
