﻿(function () {
    'use strict';

    angular
      .module('advisorsApp')
        .controller('indexCtrl', indexCtrl);

    indexCtrl.$inject = ['$state', 'authenticationService'];

    function indexCtrl($state, authenticationService) {
        var vm = this;
        vm.isCollapsed = true;
        authenticationService.fillAuthData();
        vm.userIdentity = authenticationService.userIdentity;
        vm.logout = function () {
            authenticationService.logout();
            $state.go('login');
        };

        vm.showPoster = function () { return !vm.userIdentity.isAuthenticated; }
    };

})();
