﻿(function () {
    'use strict';

    angular
        .module('advisorsApp.account')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('account', {
                url: '/profile',
                views: {
                    "mainContent": {
                        templateUrl: "www/app/account/views/profile.html"
                    }
                }
            })
            .state('settings', {
                url: '/settings',
                views: {
                    "mainContent": {
                        templateUrl: "www/app/account/views/settings.html"
                    }
                }
            });
    };
})();