﻿(function () {
    'use strict';

    angular
      .module('advisorsApp')
        .controller('accountCtrl', accountCtrl);

    accountCtrl.$inject = ['$state','$scope', '$timeout', 'authenticationService', 'alertService'];

    function accountCtrl($state,$scope, $timeout, authenticationService, alertService) {
        var vm = this;
        vm.message = "";

        //login
        vm.credentials = {
            username: "",
            password: ""
        };

        //register user
        vm.newAccount = {
            username: "",
            password: "",
            confirmPassword: ""
        };

        vm.signUp = function () {

            $scope.$broadcast('show-errors-check-validity');
            if (!$scope.signupForm.$valid) {
                return;
            }

            authenticationService.createAccount(vm.newAccount).then(onSuccess, onError);

            function onSuccess(response) {
                vm.savedSuccessfully = true;
                alertService.add(alertService.type.success, 'Account created successfully');
                $state.go('login');
            };

            function onError(response) {
                var errors = [];
                for (var key in response.data.modelState) {
                    for (var i = 0; i < response.data.modelState[key].length; i++) {
                        errors.push(response.data.modelState[key][i]);
                    }
                }
                var message = "Failed to register user due to:" + errors.join(' ');
                alertService.add(alertService.type.error, message);
            }
        };

        vm.login = function () {

            authenticationService.login(this.credentials).then(function (response) {
                $state.go('agent.list');
            },
              function (err) {
                  alertService.add(alertService.type.error, err.error_description);
              });
        };
    };

})();
