﻿
(function () {

    'use strict';

    angular
      .module('advisorsApp', [
          'ui.router',
          'ui.grid',
          'LocalStorageModule',
          'angular-loading-bar',
          'restangular',
          'ui.bootstrap',
          'validation',
          'validation.rule',
          'angular-confirm',
          'advisorsApp.agent',
          'advisorsApp.account',
      ]).config(['RestangularProvider',
                    '$httpProvider',
                    '$validationProvider',
         function (RestangularProvider, $httpProvider, $validationProvider) {

             RestangularProvider.setBaseUrl('/api');
             $httpProvider.interceptors.push('authenticationInterceptor');


             //validation settings for angular-validation module
             $validationProvider.showSuccessMessage = false;

             $validationProvider.setErrorHTML(function (msg) {
                 return "<label class=\"control-label has-error\">" + msg + "</label>";
             });
             
             //extend validation provider to use bootstrap css
             angular.extend($validationProvider, {
                 validCallback: function (element) {
                     if (element.parent() && element.parent().hasClass('has-error'))
                         element.parent().removeClass('has-error');
                 },
                 invalidCallback: function (element) {
                     if(element.parent() && element.parent().hasClass('form-group'))
                        element.parent().addClass('has-error');
                 }
             });
         }]);
})();