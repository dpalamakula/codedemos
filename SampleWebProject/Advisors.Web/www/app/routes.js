﻿(function () {
    'use strict';

    angular
        .module('advisorsApp')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: "/home",
                views: {
                    "mainContent": {
                        templateUrl: "www/app/home/home.html"
                    }
                }
            })
            .state('login', {
                url: '/login',
                views: {
                    "mainContent": {
                        templateUrl: "www/app/authentication/views/login.html"
                    }
                }
            }).state('signup', {
                url: '/signup',
                views: {
                    'mainContent': {
                        templateUrl: "www/app/authentication/views/signup.html"
                    }
                }
            });

        $urlRouterProvider.otherwise('/home');
    };
})();