﻿(function () {
    'use strict';

    angular
      .module('advisorsApp')
        .factory('alertService', alertService);

    alertService.$inject = ['$rootScope', '$timeout'];

    function alertService($rootScope, $timeout) {
        var alertService = {};

        alertService.type = {
            error: 'danger',
            success: 'success'
        };

        $rootScope.alerts = [];

        alertService.add = function (type, msg) {
            $rootScope.alerts.push({
                type: type,
                msg: msg,
                close: function ()
                {
                    alertService.close(this);
                }
            });

            $timeout(function () {
                alertService.close(this);
            }, 3000);
        };

        alertService.close = function (alert) {
            var index = $rootScope.alerts.indexOf(alert);
            $rootScope.alerts.splice(index, 1);
        };

        return alertService;
    };
})();