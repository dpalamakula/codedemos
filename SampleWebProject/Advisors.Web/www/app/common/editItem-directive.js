﻿(function () {
    'use strict';

    angular
      .module('advisorsApp')
        .directive('editItem', editItem);

    editItem.$inject = [];

    function editItem() {
        return {
            restrict: 'A',
            scope: {
                editItem: '@'
            },
            replace: 'true',
            templateUrl: '/www/app/common/views/editItem.html'
        };
    };
})();