﻿(function () {
    'use strict';

    angular
      .module('advisorsApp')
        .directive('deleteItem', deleteItem);

    deleteItem.$inject = ['$confirm'];

    function deleteItem($confirm) {
        return {
            restrict: 'E',
            scope: {
                callback: '&'
            },
            controller: function ($confirm, $scope, $location) {
                $scope.showConfirmation = function () {
                    $confirm({ text: 'Are you sure you want to delete?' })
                .then(function () {
                    $scope.callback();
                });
                };
            },
            replace: 'false',
            templateUrl: '/www/app/common/views/deleteItem.html'
        };
    };
})();