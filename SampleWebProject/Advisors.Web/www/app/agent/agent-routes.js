﻿(function () {
    'use strict';

    angular
        .module('advisorsApp.agent')
        .config(routeConfig);

    routeConfig.$inject = ['$stateProvider'];

    function routeConfig($stateProvider) {
        $stateProvider
            .state('agent', {
                url: '/agent',
                views: {
                    "mainContent": {
                        templateUrl: "www/app/agent/views/agent.html"
                    }
                }
            })
            .state('agent.list', {
                url:'/list',
                templateUrl: "www/app/agent/views/list.html"
            })
            .state('agent.detail', {
                url: '/detail',
                templateUrl: "www/app/agent/views/detail.html"
                
            }).state('agent.edit', {
                url: '/edit/:id',
                templateUrl: "www/app/agent/views/add.html"

            }).state('agent.add', {
                url: '/add',
                templateUrl: "www/app/agent/views/add.html"
                
            });

    };
})();