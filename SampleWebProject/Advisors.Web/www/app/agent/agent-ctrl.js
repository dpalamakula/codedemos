﻿(function () {
    'use strict';

    angular
      .module('advisorsApp.agent')
        .controller('agentCtrl', agentCtrl);

    agentCtrl.$inject = ['$state', '$stateParams', 'agentService', 'alertService', '$confirm'];

    function agentCtrl($state, $stateParams, agentService, alertService, $confirm) {
        var vm = this;

        vm.IsEditMode = false;

        vm.newAgent = {
            firstName: "",
            lastName: "",
            email: "",
            phone: ""
        };

        vm.selectedAgentId = Number($stateParams.id);

        if (vm.selectedAgentId > 0) {
            agentService.one(vm.selectedAgentId).get().then(function (agent) {
                vm.IsEditMode = true;
                vm.newAgent = agent;
            }, function (error) {
                alertService.add(alertService.type.error, error);
            });
        }

        //methods
        vm.addAgent = function (id) {
            agentService.post(vm.newAgent).then(function (newResource) {
                $state.go('agent.list');
            });
        };

        vm.updateAgent = function () {
            vm.newAgent.put().then(function (data) {
                $state.go('agent.list');
            }, function (error) {
                alertService.add(alertService.type.error, error);
            });
        };

        vm.saveAgent = function() {
            if (vm.IsEditMode)
                vm.updateAgent();
            else
                vm.addAgent();
        };

    }

})();
