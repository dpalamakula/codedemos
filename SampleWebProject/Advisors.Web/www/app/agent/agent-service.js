﻿(function () {
    'use strict';

    angular
      .module('advisorsApp.agent')
        .factory('agentService', agentService);

    agentService.$inject = ['$http', '$q', '$log', 'Restangular'];

    function agentService($http, $q, $log, Restangular) {
        var agentsSvc = Restangular.service('agent');
        return agentsSvc;
    };
})();
