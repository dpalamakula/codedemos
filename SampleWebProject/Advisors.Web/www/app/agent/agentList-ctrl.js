﻿(function () {
    'use strict';

    angular
      .module('advisorsApp.agent')
        .controller('agentListCtrl', agentListCtrl);

    agentListCtrl.$inject = ['$scope','$timeout', '$state', 'agentService', 'alertService', '$confirm'];

    function agentListCtrl($scope, $timeout, $state, agentService, alertService, $confirm) {
        var vm = this;

        vm.agentFilter = {
            firstName: "",
            lastName: "",
            email: "",
            pageNumber: 1,
            pageSize: 10
        };

        vm.newAgent = {
            firstName: "",
            lastName: "",
            email: "",
            phone: ""
        };

        var editTemplate = '<a href="#/agent/edit/{{row.entity.id}}"><i class="glyphicon glyphicon-pencil" rel="tooltip" title="Edit"></i></a>';
        var deleteTemplate = '<a ng-click="grid.appScope.deleteAgent(row.entity)"><i class="glyphicon glyphicon-remove" rel="tooltip" title="Delete"></i></a>';

        vm.gridOptions = {
            enableSorting: true,
            enableFiltering: true,
            useExternalFiltering: true,
            columnDefs: [
                { name: 'firstName', field: 'firstName' },
                { name: 'lastName', field: 'lastName' },
                { name: 'email', field: 'email' },
                { name: 'phone', field: 'phone', enableFiltering: false },
                { name: 'Actions', displayName: '', enableSorting: false, enableFiltering: false, cellTemplate: editTemplate + '&nbsp;' + deleteTemplate }
            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.core.on.filterChanged($scope, function () {
                    var grid = this.grid;
                    vm.agentFilter.firstName = grid.columns[0].filters[0].term;
                    vm.agentFilter.lastName = grid.columns[1].filters[0].term;
                    vm.agentFilter.email = grid.columns[2].filters[0].term;
                    
                    if (angular.isDefined($scope.filterTimeout)) {
                        $timeout.cancel($scope.filterTimeout);
                    }
                    $scope.filterTimeout = $timeout(function () {
                        vm.loadAgents();
                    }, 300);
                });
            }
        };

        //for ui-grid this has to be on scope
        $scope.deleteAgent = function (entity) {
            $confirm({ text: 'Are you sure you want to delete?' })
            .then(function () {
                agentService.one(entity.id).remove().then(function (response) {
                    var index = vm.gridOptions.data.indexOf(entity);
                    vm.gridOptions.data.splice(index, 1);
                }, function (error) {
                    alertService.add(alertService.type.error, error);
                });
            });
        };

        //load all agents
        vm.loadAgents = function () {
            agentService.getList(vm.agentFilter, { 'Content-Type': 'application/json; charset=UTF-8' }).then(function (agents) {
                vm.gridOptions.data = agents;
            }, function (error) {
                alertService.add(alertService.type.error, error);
            });
        };

        vm.loadAgents();

        $scope.$on("$destroy", function (event) {
            if (angular.isDefined($scope.filterTimeout)) {
                $timeout.cancel($scope.filterTimeout);
            }
        });
    }

})();
