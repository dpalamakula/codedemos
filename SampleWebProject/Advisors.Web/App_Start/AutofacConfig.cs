﻿using System.Reflection;
using System.Web.Http;
using Advisors.Domain.Interfaces.Repository;
using Advisors.Domain.Interfaces.Services;
using Advisors.Domain.Interfaces.UnitOfWork;
using Advisors.Domain.Models;
using Advisors.Domain.Services;
using Advisors.Infrastructure;
using Advisors.Infrastructure.DataContext;
using Advisors.Infrastructure.Repository;
using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Advisors.Web.App_Start
{
    public class AutofacConfig
    {
        public static void RegisterComponents(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CommandRepository<Agent, int>>().As<ICommandRepository<Agent, int>>();
            builder.RegisterType<QueryRepository<Agent, int>>().As<IQueryRepository<Agent, int>>();


            builder.RegisterType<EfUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();

            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<AuthContext>().As<IdentityDbContext<IdentityUser>>().InstancePerLifetimeScope();
            builder.RegisterType<AuthRepository>().As<IAuthRepository>().InstancePerLifetimeScope();
            builder.RegisterType<AdvisorsContext>().As<IDataContextAsync>().InstancePerLifetimeScope();
            builder.RegisterType<AgentService>().As<IAgentService>().InstancePerLifetimeScope();
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerLifetimeScope();


            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}