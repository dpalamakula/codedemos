﻿using System;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Advisors.Domain.Models;
using Advisors.Infrastructure;
using Advisors.Infrastructure.Repository;
using Advisors.Infrastructure.Security;
using Advisors.Web.App_Start;
using Autofac.Integration.WebApi;
using Microsoft.Owin.Security.OAuth;

namespace Advisors.Web
{
    [assembly: OwinStartup(typeof(Advisors.Web.Startup))]
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            ConfigureOAuth(app);

            AutofacConfig.RegisterComponents(config);
            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AuthorizationProvider()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

        }
    }
}