namespace Advisors.Web.Migrations
{
    using Advisors.Domain.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Advisors.Infrastructure.DataContext.AdvisorsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Advisors.Infrastructure.DataContext.AdvisorsContext context)
        {
            //  This method will be called after migrating to the latest version.
            var agents = new List<Agent>();

            for (int i = 0; i < 10; i++)
            {
                agents.Add(
                    new Agent
                    {
                        Id = i + 1,
                        FirstName = String.Format("First{0}", i),
                        LastName = String.Format("Last{0}", i),
                        Email = String.Format("random{0}@gmail.com", i),
                        Phone = "123456789",
                        CommisionPercentage = 5.36m * (i + 1)
                    }
                );
            }

            context.Agents.AddOrUpdate(
                  p => p.Id,
                  agents.ToArray()
                );

        }
    }
}
