﻿using System;
using System.Web.Http;
using Advisors.Domain.Dto;
using Advisors.Domain.Interfaces.Services;
using Advisors.Domain.Models;
using Advisors.Domain.Services;

namespace Advisors.Web.Controllers
{
    [RoutePrefix("api/agent")]
    public class AgentController : BaseApiController
    {
        private readonly IAgentService _agentService;

        public AgentController(IAgentService agentService)
        {
            _agentService = agentService;
        }

        [Route("")]
        public IHttpActionResult Get([FromUri]AgentQuery query)
        {
            try
            {
                return Ok(_agentService.GetAgents(query));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Get(int id)
        {
            try
            {
                Agent agent = null;
                if (id > 0)
                {
                    agent = _agentService.GetAgent(id);
                    if (agent == null)
                    {
                        return NotFound();
                    }
                }
                return Ok(agent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Post([FromBody] Agent agent)
        {
            try
            {
                if (agent == null)
                {
                    return BadRequest("Agent cannot be null");
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _agentService.AddAgent(agent);
                return Created<Agent>(Request.RequestUri + agent.Id.ToString(),
                    agent);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Put(int id, [FromBody] Agent agent)
        {
            try
            {
                if (agent == null || id != agent.Id)
                {
                    return BadRequest("Agent cannot be null");
                }


                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _agentService.UpdateAgent(agent);

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult Delete(int id)
        {
            try
            {
                var agent = _agentService.GetAgent(id);
                if (agent == null)
                {
                    return NotFound();
                }

                _agentService.DeleteAgent(agent);
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}