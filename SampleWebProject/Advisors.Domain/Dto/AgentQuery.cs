﻿using System;
using System.Linq.Expressions;
using Advisors.Domain.Models;
using LinqKit;

namespace Advisors.Domain.Dto
{
    public class AgentQuery : SortAndPageQuery, ISearchFilter<Agent>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        protected override string DefaultSortColumn
        {
            get { return "FirstName"; }
        }

        public Expression<Func<Agent, bool>> Filter()
        {
            var predicate = PredicateBuilder.True<Agent>();
            if (!string.IsNullOrEmpty(FirstName))
                predicate = predicate.And(p => p.FirstName.Contains(FirstName));

            if (!string.IsNullOrEmpty(LastName))
                predicate = predicate.And(p => p.LastName.Contains(LastName));

            if (!string.IsNullOrEmpty(Email))
                predicate = predicate.And(p => p.Email.Contains(Email));

            predicate.Compile();
            return predicate;
        }
    }
}