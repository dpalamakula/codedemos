﻿using Advisors.Domain.Models;

namespace Advisors.Domain.Dto
{
    public class InitiatePolicyCommand
    {
        public int AgentId { get; set; }
        public int CustomerId { get; set; }
        public Policy PolicyProduct { get; set; }
    }
}
