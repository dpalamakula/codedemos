﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Advisors.Domain.Dto
{
    public interface ISearchFilter<TEntity>
    {
        Expression<Func<TEntity, bool>> Filter();
    }
}
