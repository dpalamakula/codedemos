﻿using System;

namespace Advisors.Domain.Dto
{
    public class PagedQuery
    {
        public int PageSize { get; set; }
        public int PageNumber { get; set; }

        public int Skipsize
        {
            get { return PageSize*(PageNumber-1); }
        }

        public PagedQuery()
        {
            this.PageNumber = 1;
            this.PageSize = 10;
        }

        public PagedQuery(int pageNumber, int pageSize)
        {
            if(pageNumber < 0 || pageSize < 0)
                throw new ArgumentOutOfRangeException();

            PageSize = pageSize;
            PageNumber = pageNumber;
        }


    }
}
