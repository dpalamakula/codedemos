﻿using System.Collections.Generic;
using Advisors.Domain.Models;

namespace Advisors.Domain.Dto
{
    public class PagedResult<TEntity, TKey> where TEntity: Entity<TKey>
    {
        private readonly IEnumerable<TEntity> _items;
        private readonly int _totalCount;

        public PagedResult(IEnumerable<TEntity> items, int totalCount)
        {
            _items = items;
            _totalCount = totalCount;
        }

        public IEnumerable<TEntity> Items { get { return _items; } }
        public int TotalCount { get { return _totalCount; } }
    }
}
