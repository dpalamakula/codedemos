﻿using System;

namespace Advisors.Domain.Dto
{
    public abstract class SortAndPageQuery : PagedQuery
    {
        private string _sortColumn;

        public enum SortOrder
        {
            Ascending,
            Descending
        }

        protected abstract string DefaultSortColumn { get; }

        public string SortColumn
        {
            get
            {
                return String.IsNullOrEmpty(_sortColumn) ? DefaultSortColumn : _sortColumn;
            }
            set { _sortColumn = value; }
        }

        public SortOrder ColumnOrder { get; set; }
    }
}