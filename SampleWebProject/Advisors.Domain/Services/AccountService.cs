﻿using System.Threading.Tasks;
using Advisors.Domain.Interfaces.Repository;
using Advisors.Domain.Interfaces.Services;
using Advisors.Domain.Models;
using Microsoft.AspNet.Identity;

namespace Advisors.Domain.Services
{
    public class AccountService:IAccountService
    {
        private IAuthRepository _authRepository;

        public AccountService(IAuthRepository authRepository)
        {
            _authRepository = authRepository;
        }

        public async Task<IdentityResult> CreateAccount(UserAccount account)
        {
            var result = await _authRepository.RegisterUser(account);
            return result;
        }
    }
}
