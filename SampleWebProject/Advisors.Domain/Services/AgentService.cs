﻿using System;
using System.Collections.Generic;
using System.Linq;
using Advisors.Domain.Dto;
using Advisors.Domain.Interfaces.Repository;
using Advisors.Domain.Interfaces.Services;
using Advisors.Domain.Interfaces.UnitOfWork;
using Advisors.Domain.Models;
using Advisors.Domain.QueryExtensions;
using LinqKit;

namespace Advisors.Domain.Services
{
    public class AgentService : IAgentService
    {
        private readonly ICommandRepository<Agent, int> _agentCommandRepository;
        private readonly IQueryRepository<Agent, int> _agentQueryRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AgentService(
            IQueryRepository<Agent, int> agentQueryRepository,
            ICommandRepository<Agent, int> agentCommandRepository,
            IUnitOfWork unitOfWork)
        {
            _agentQueryRepository = agentQueryRepository;
            _agentCommandRepository = agentCommandRepository;
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Agent> GetAgents(AgentQuery agentQuery)
        {
            var agentQueryable = _agentQueryRepository.GetAll().AsExpandable()
                .Where(agentQuery.Filter()) as IOrderedQueryable<Agent>;

            return agentQueryable.ApplySortingPaging(agentQuery);
        }

        public Agent GetAgent(int id)
        {
            return _agentQueryRepository.GetById(id);
        }

        public void AddAgent(Agent agent)
        {
            _agentCommandRepository.Add(agent);
            _unitOfWork.SaveChanges();
        }

        public void UpdateAgent(Agent agent)
        {
            _agentCommandRepository.Update(agent);
            _unitOfWork.SaveChanges();
        }

        public void DeleteAgent(Agent agent)
        {
            _agentCommandRepository.Delete(agent);
            _unitOfWork.SaveChanges();
        }

        public void InitiatePolicy(InitiatePolicyCommand policyCommand)
        {
            throw new NotImplementedException();
        }
    }
}