namespace Advisors.Domain.Models
{
    public enum ObjectState
    {
        Unchanged,
        Added,
        Modified,
        Deleted
    }

    public interface IObjectState
    {
        ObjectState ObjectState { get; set; }
    }
}