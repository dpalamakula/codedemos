﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advisors.Domain.Models
{
    public class Entity<TKey>:IObjectState
    {
        //public TKey Id { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}