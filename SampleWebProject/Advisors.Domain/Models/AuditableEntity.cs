﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advisors.Domain.Models
{
    public class AuditableEntity<TKey>:Entity<TKey>
    {
        public AuditableEntity()
        {
            AddedDate = DateTime.UtcNow;
            ModifiedDate = DateTime.UtcNow;
        }

        public DateTime AddedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
