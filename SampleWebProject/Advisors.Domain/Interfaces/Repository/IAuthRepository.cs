﻿using System;
using System.Threading.Tasks;
using Advisors.Domain.Models;
using Microsoft.AspNet.Identity;

namespace Advisors.Domain.Interfaces.Repository
{
    public interface IAuthRepository : IDisposable
    {
        Task<IdentityResult> RegisterUser(UserAccount userModel);
        void Dispose();
    }
}