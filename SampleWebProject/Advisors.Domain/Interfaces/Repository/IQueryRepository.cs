﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Advisors.Domain.Models;

namespace Advisors.Domain.Interfaces.Repository
{
    public interface IQueryRepository<TEntity, in TKey> where TEntity : Entity<TKey>
    {
        IQueryable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> GetAll();
        TEntity GetById(TKey id);
    }
}