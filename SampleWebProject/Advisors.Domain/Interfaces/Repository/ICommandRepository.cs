﻿using Advisors.Domain.Models;

namespace Advisors.Domain.Interfaces.Repository
{
    public interface ICommandRepository<in TEntity, TKey> where TEntity : Entity<TKey>
    {
        void Add(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}