﻿using Advisors.Domain.Models;

namespace Advisors.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity, TKey> : IQueryRepository<TEntity, TKey>, ICommandRepository<TEntity, TKey>
        where TEntity : Entity<TKey>
    {
    }
}