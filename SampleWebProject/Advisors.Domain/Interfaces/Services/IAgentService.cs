using System.Collections.Generic;
using Advisors.Domain.Dto;
using Advisors.Domain.Models;

namespace Advisors.Domain.Interfaces.Services
{
    public interface IAgentService
    {
        IEnumerable<Agent> GetAgents(AgentQuery query);
        Agent GetAgent(int id);
        void AddAgent(Agent agent);
        void UpdateAgent(Agent agent);
        void DeleteAgent(Agent agent);
        void InitiatePolicy(InitiatePolicyCommand policyCommand);
    }
}