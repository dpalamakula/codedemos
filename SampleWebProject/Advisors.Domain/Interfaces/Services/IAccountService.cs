﻿using System.Threading.Tasks;
using Advisors.Domain.Models;
using Microsoft.AspNet.Identity;

namespace Advisors.Domain.Interfaces.Services
{
    public interface IAccountService
    {
        Task<IdentityResult> CreateAccount(UserAccount account);
    }
}
