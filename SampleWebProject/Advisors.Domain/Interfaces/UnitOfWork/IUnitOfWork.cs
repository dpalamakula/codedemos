﻿using System;
using System.Threading.Tasks;

namespace Advisors.Domain.Interfaces.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
    }
}
