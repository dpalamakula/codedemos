Setup Steps:
1. Set Advisors.Web as startup project and build the solution
2. Open package manager console Tools->Library Package Manager -> Package Manager Console. Change default project to Advisors.Web.
3. Execute the command Update-Database. This will create tables and data for sample project
4. Login or Register functionality will invoke the Asp Identity setup and all related tables are created the very first time.

***If there are issues with other step with Update-Database command you might have to delete packages folder rebuild and reopen visual studio

http://stackoverflow.com/questions/9674983/the-term-update-database-is-not-recognized-as-the-name-of-a-cmdlet

Advisors.Web
-----------------------
This project has both the web api controller and the angularJS client app. All angularJS app related 
script files(angular sub modules, controllers, services etc) are under www/app. Ideally for a large app follow the fractal hierarchy
where each feature area is like a mini angular module. Feature might have sub-features so mini angular module might have sub mini angular modules.
The third party libraries
used by the app are under www/assets/lib. The css/less, fonts and images used by the app are under www/assets/css, 
www/assets/less, www/assets/fonts and www/assets/images. This project uses EF database migrations 
which are in the migrations folder.

 
 References:
 http://bitoftech.net/2014/06/09/angularjs-token-authentication-using-asp-net-web-api-2-owin-asp-net-identity/
 https://github.com/angular-ui/ui-router
 https://github.com/mgonto/restangular
 https://angular-ui.github.io/bootstrap/
 http://angular-ui.github.io/angular-google-maps/#!/
 http://bootswatch.com/
 https://github.com/huei90/angular-validation
 http://ui-grid.info/


