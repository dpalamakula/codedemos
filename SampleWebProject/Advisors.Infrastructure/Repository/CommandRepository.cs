﻿using System.Data.Entity;
using Advisors.Domain.Interfaces.Repository;
using Advisors.Domain.Models;
using Advisors.Infrastructure.DataContext;

namespace Advisors.Infrastructure.Repository
{
    public class CommandRepository<TEntity, TKey> : ICommandRepository<TEntity, TKey> where TEntity : Entity<TKey>
    {
        private readonly IDataContextAsync _context;
        private readonly DbSet<TEntity> _dbSet;

        public CommandRepository(IDataContextAsync context)
        {
            _context = context;

            var dbContext = context as DbContext;

            if (dbContext != null)
            {
                _dbSet = dbContext.Set<TEntity>();
            }
        }

        public void Add(TEntity entity)
        {
            entity.ObjectState = ObjectState.Added;
            _dbSet.Attach(entity);
            _context.SyncObjectState(entity);
        }

        public virtual void Update(TEntity entity)
        {
            entity.ObjectState = ObjectState.Modified;
            _dbSet.Attach(entity);
            _context.SyncObjectState(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            entity.ObjectState = ObjectState.Deleted;
            _dbSet.Attach(entity);
            _context.SyncObjectState(entity);
        }

        public virtual void Delete(object id)
        {
            var entity = _dbSet.Find(id);
            Delete(entity);
        }
    }
}