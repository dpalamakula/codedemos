﻿using System;
using System.Threading.Tasks;
using Advisors.Domain.Interfaces.Repository;
using Advisors.Domain.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Advisors.Infrastructure.Repository
{
    public class AuthRepository : IAuthRepository
    {
        private IdentityDbContext<IdentityUser> _authContext;

        private UserManager<IdentityUser> _userManager;

        public AuthRepository(IdentityDbContext<IdentityUser> authContext)
        {
            _authContext = authContext;
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_authContext));
        }

        public async Task<IdentityResult> RegisterUser(UserAccount userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            return result;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public void Dispose()
        {
            _authContext.Dispose();
            _userManager.Dispose();

        }
    }
}
