﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Advisors.Domain.Interfaces.Repository;
using Advisors.Domain.Models;
using Advisors.Infrastructure.DataContext;

namespace Advisors.Infrastructure.Repository
{
    public class QueryRepository<TEntity,TKey> : IQueryRepository<TEntity, TKey> where TEntity : Entity<TKey>
    {
        private IDataContextAsync _context;
        private readonly DbSet<TEntity> _dbSet;

        public QueryRepository(IDataContextAsync context)
        {
            _context = context;

            var dbContext = context as DbContext;

            if (dbContext != null)
            {
                _dbSet = dbContext.Set<TEntity>();
            }
        }

        public IQueryable<TEntity> SearchFor(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public TEntity GetById(TKey id)
        {
            return _dbSet.Find(id);
        }
    }
}
