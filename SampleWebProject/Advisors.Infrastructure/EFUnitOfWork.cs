﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advisors.Domain.Interfaces.UnitOfWork;
using Advisors.Infrastructure.DataContext;

namespace Advisors.Infrastructure
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly IDataContextAsync _context;
        private bool _disposed;

        public EfUnitOfWork(IDataContextAsync context)
        {
            _context = context;
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            var result = await _context.SaveChangesAsync();
            return result;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
