﻿using System.Security.Claims;
using System.Threading.Tasks;
using Advisors.Infrastructure.DataContext;
using Advisors.Infrastructure.Repository;
using Microsoft.Owin.Security.OAuth;

namespace Advisors.Infrastructure.Security
{
    public class AuthorizationProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] {"*"});

            using (var authRepository = new AuthRepository(new AuthContext()))
            {
                var user = await authRepository.FindUser(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("invalid_account", "The user name or password is incorrect.");
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim("sub", context.UserName));
            identity.AddClaim(new Claim("role", "user"));
            //code to add additional claims
            context.Validated(identity);
        }
    }
}