﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Advisors.Infrastructure.DataContext
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("AdvisorsConnectionString")
        {

        }
    }
}
