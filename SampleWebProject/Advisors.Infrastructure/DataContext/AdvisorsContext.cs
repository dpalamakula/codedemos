﻿using System.Data.Entity;
using Advisors.Domain.Models;
using Advisors.Infrastructure.EntityMapping;

namespace Advisors.Infrastructure.DataContext
{
    public class AdvisorsContext:BaseDataContext
    {
        static AdvisorsContext()
        {
            Database.SetInitializer<AdvisorsContext>(null);
        }

        public AdvisorsContext()
            : base("AdvisorsConnectionString")
        {
            
        }

        public DbSet<Agent> Agents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AgentMap());
        }
    }
}
