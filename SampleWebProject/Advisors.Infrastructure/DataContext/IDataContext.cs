﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Advisors.Domain.Models;

namespace Advisors.Infrastructure.DataContext
{
    public interface IDataContext : IDisposable
    {
        int SaveChanges();
        void SyncObjectState<TEntity>(TEntity entity) where TEntity : class, IObjectState;
        void SyncObjectsStatePostCommit();
    }
}
