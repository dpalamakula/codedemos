﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advisors.Domain.Models;

namespace Advisors.Infrastructure.EntityMapping
{
    public class AgentMap : EntityTypeConfiguration<Agent>
    {
        public AgentMap()
        {
            // Primary Key
            this.HasKey(t => t.Id).Property(c => c.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Properties
            this.Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(256);

            this.Property(t => t.Email)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Agents");
        }
    }
}
