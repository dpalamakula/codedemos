﻿using DataAccessLayer;
using Entities;

namespace BusinessLayer
{
    public class UserProvider
    {
        private readonly UserDao _userDao = new UserDao();

        public void UpdateUser(User user)
        {
            _userDao.SaveUser(user);
        }
    }
}
