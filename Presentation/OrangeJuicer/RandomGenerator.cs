using System;

namespace OrangeSupreme
{
    public class RandomGenerator
    {
        private readonly Random _rnd = new Random(2);
        private static RandomGenerator _instance;
        private RandomGenerator()
        {
            
        }
        public double RandomJuiceAmount
        {
            get { return _rnd.Next(1, 5); }
        }

        public int RandomPhLevel
        {
            get { return _rnd.Next(2,7); }
        }

        public int RandomOrangeSize
        {
            get { return _rnd.Next(1, 30); }
        }

        public static RandomGenerator Instance
        {
            get
            {
                return _instance ?? (_instance = new RandomGenerator());
            }
        }
    }
}