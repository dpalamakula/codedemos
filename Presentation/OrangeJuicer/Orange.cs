﻿using System;

namespace OrangeSupreme
{
    public class Orange
    {
        public int Diameter { get; private set; }
        public int PhLevel { get; private set; }
        public double JuiceLevel { get; private set; }

        public Orange()
        {
            Diameter = RandomGenerator.Instance.RandomOrangeSize;
            PhLevel = RandomGenerator.Instance.RandomPhLevel;
            JuiceLevel = RandomGenerator.Instance.RandomJuiceAmount;
        }

        public double ExtractJuice()
        {
            if (!IsOrangeSizeAcceptable())
                throw new Exception("Orange is over sized");

            if(!IsPhLevelAcceptable())
                throw new Exception("Orange is rotten");

            return Extract();
        }

        /// <summary>
        /// RULE - Orange of size greater than 20 are not allowed
        /// </summary>
        /// <returns></returns>
        private bool IsOrangeSizeAcceptable()
        {
            return Diameter <= 20;
        }
        /// <summary>
        /// RULE - ph Level for orange should be between 2.9 and 4.0.
        /// </summary>
        /// <returns></returns>
        private bool IsPhLevelAcceptable()
        {
            return PhLevel >= 2 && PhLevel <= 6;
        }

        private double Extract()
        {
            return JuiceLevel;
        }
    }
}