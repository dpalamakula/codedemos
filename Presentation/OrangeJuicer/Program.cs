﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace OrangeSupreme
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine("Enter number of Oranges :");
            var userInput = Console.ReadLine();
            var total = Convert.ToInt32(userInput);

            var oranges = new List<Orange>();
            while (total > 0)
            {
                oranges.Add(new Orange());
                total--;
            }

            var juiceQuantity = 0d;
            var counter = 0;
            var goodOranges = 0;
            foreach (var orange in oranges)
            {
                counter++;
                try
                {
                    Console.Out.Write("# {0} : ",counter);


                    juiceQuantity = juiceQuantity + orange.ExtractJuice();
                    if (juiceQuantity >= 20)
                    {
                        Console.Out.WriteLine("[ OVERSPILL ] - stopping extraction");
                        break;
                    }

                    Thread.Sleep(200);
                    Console.Out.WriteLine("[OK]");

                    goodOranges++;
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine(ex.Message);
                }
            }

            Console.Out.WriteLine("");
            Console.Out.WriteLine("Total Juice extract out of {0}/{1} oranges is {2} ml",goodOranges, oranges.Count, juiceQuantity);
            Console.ReadKey();
        }
    }
}
