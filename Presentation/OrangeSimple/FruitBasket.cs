﻿using System.Collections.Generic;
using OrangeSimple.Compenents;

namespace OrangeSimple
{
    public class FruitBasket
    {
        private int _totalfruits;

        public FruitBasket(int totalfruits)
        {
            _totalfruits = totalfruits;
        }

        public IEnumerable<ICitrusFruit> PickupFruit()
        {
            while (_totalfruits > 0)
            {
                _totalfruits--;
                yield return new Orange();
            }
        } 
    }
}