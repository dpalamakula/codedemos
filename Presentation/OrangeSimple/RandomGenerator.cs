﻿using System;

namespace OrangeSimple
{
    public class RandomGenerator
    {
        private readonly Random _rnd = new Random(2);
        private static RandomGenerator _instance;
        private RandomGenerator()
        {

        }
        public double RandomJuiceAmount
        {
            get { return _rnd.NextDouble()*_rnd.Next(3,8); }
        }

        public int RandomPhLevel
        {
            get { return _rnd.Next(1, 8); }
        }

        public int RandomOrangeSize
        {
            get { return _rnd.Next(1, 30); }
        }

        public static RandomGenerator Instance
        {
            get
            {
                return _instance ?? (_instance = new RandomGenerator());
            }
        }
    }
}