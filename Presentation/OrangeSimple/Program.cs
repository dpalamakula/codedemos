﻿using System;
using OrangeSimple.Compenents;

namespace OrangeSimple
{
    class Program
    {
        static void Main()
        {
            while (true)
            {
                Console.Out.WriteLine("Enter number of Oranges :");
                var userInput = Console.ReadLine();
                var total = Convert.ToInt32(userInput);
                var basket = new FruitBasket(total);

                IJuicer extractor = new OrangeJuicer(new Marc1000OrangePartsJuicer());
                foreach (var orange in basket.PickupFruit())
                {
                    extractor.ExtractJuice(orange);
                }
            
                Console.Out.WriteLine("Total Juice extract out of {0}/{1} oranges is {2} ml", 
                    extractor.TotalItemRejected, 
                    extractor.TotalItems, 
                    extractor.JuiceQuantity);

                Console.ReadKey();  
                Console.Clear();
            }
        }
    }
}
