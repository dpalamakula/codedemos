﻿using System;

namespace OrangeSimple.Compenents
{
    public class OrangeJuicer : IJuicer
    {
        public int TotalItems { get; private set; }
        public int TotalItemProcessed { get; private set; }
        public int TotalItemRejected { get; private set; }
        public double JuiceQuantity { get { return _juicer.GetJuicerContainer().JuiceQuantity; } }

        private readonly OrangeJuicerPartsFactory _juicer;
        public OrangeJuicer(OrangeJuicerPartsFactory juicer)
        {
            _juicer = juicer;
        }

        public void ExtractJuice(ICitrusFruit orange)
        {
            if (_juicer.GetGrinder().Grid(orange))
                TotalItemProcessed++;
            else
                TotalItemRejected++;

            TotalItems++;
            Console.Out.WriteLine("{0} orange - [{1}]", TotalItems, _juicer.GetGrinder().DisplayMessage);
        } 
    }
}