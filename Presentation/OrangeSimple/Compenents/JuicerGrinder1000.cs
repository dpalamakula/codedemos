﻿using System.Threading;

namespace OrangeSimple.Compenents
{
    public class JuicerGrinder1000 : IGrinder
    {
        private readonly IJuiceContainer _container;
        private readonly IJuicerQualitySensor _sensor;
        
        public string DisplayMessage { get; private set; }
        public JuicerGrinder1000(IJuicerQualitySensor sensor,IJuiceContainer container)
        {
            _sensor = sensor;
            _container = container;
        }

        public bool Grid(ICitrusFruit orange)
        {
            Thread.Sleep(200);
            if (!_container.IsFull)
            {
                var message = _sensor.IsGoodToGrid(orange);
                if (!string.IsNullOrEmpty(message))
                {
                    DisplayMessage = message;
                    return false;
                }

                _container.AddJuice(orange.JuiceLevel);
                DisplayMessage = "OK";
                return true;
            }
            
            DisplayMessage = "Overflow Detected";
            return false;
        }
    }
}