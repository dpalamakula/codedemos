﻿using System;

namespace OrangeSimple.Compenents
{
    public class JuiceContainer20Liter : IJuiceContainer
    {
        public double JuiceQuantity { get; private set; }
        public bool IsFull { get { return JuiceQuantity >= 20; } }

        public void AddJuice(double amount)
        {
            if (IsFull)
            {
                Console.Out.WriteLine("Container is FULL");
                return;
            }

            JuiceQuantity = Math.Round(JuiceQuantity  + amount,2);
        }
    }
}