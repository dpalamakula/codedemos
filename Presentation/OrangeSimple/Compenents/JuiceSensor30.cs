namespace OrangeSimple.Compenents
{
    public class JuicerQualitySensor30 : IJuicerQualitySensor
    {
        public string IsGoodToGrid(ICitrusFruit orange)
        {
            if (!IsOrangeSizeAcceptable(orange))
            {
                return "Oversized";
            }

            if (!IsRotten(orange))
            {
                return "Rotten";
            }

            return null;
        }

        private static bool IsRotten(ICitrusFruit orange)
        {
            return orange.PhLevel >= 3 && orange.PhLevel <= 6;
        }

        private static bool IsOrangeSizeAcceptable(ICitrusFruit orange)
        {
            return orange.Diameter <= 20;
        }
    }
}