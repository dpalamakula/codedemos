﻿namespace OrangeSimple.Compenents
{
    public interface IJuicer
    {
        int TotalItems { get; }
        int TotalItemProcessed { get; }
        int TotalItemRejected { get; }
        double JuiceQuantity { get; }

        void ExtractJuice(ICitrusFruit orange);
    }

    public interface IGrinder
    {
        string DisplayMessage { get; }
        bool Grid(ICitrusFruit orange);
    }

    public interface IJuiceContainer
    {
        double JuiceQuantity { get; }
        bool IsFull { get; }
        void AddJuice(double amount);
    }

    public interface IJuicerQualitySensor
    {
        string IsGoodToGrid(ICitrusFruit orange);
    }
    
    public interface ICitrusFruit
    {
        int Diameter { get; }
        int PhLevel { get; }
        double JuiceLevel { get; }
    }
}