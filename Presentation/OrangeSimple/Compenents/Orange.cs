﻿namespace OrangeSimple.Compenents
{
    public class Orange : ICitrusFruit
    {
        public int Diameter { get; private set; }
        public int PhLevel { get; private set; }
        public double JuiceLevel { get; private set; }

        public Orange()
        {
            Diameter = RandomGenerator.Instance.RandomOrangeSize;
            PhLevel = RandomGenerator.Instance.RandomPhLevel;
            JuiceLevel = RandomGenerator.Instance.RandomJuiceAmount;
        }
    }
}