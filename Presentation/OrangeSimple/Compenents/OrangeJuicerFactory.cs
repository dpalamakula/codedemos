﻿namespace OrangeSimple.Compenents
{
    public abstract class OrangeJuicerPartsFactory
    {
        public abstract IGrinder GetGrinder();
        public abstract IJuicerQualitySensor GetSensor();
        public abstract IJuiceContainer GetJuicerContainer();
    }

    public class Marc1000OrangePartsJuicer : OrangeJuicerPartsFactory
    {
        private readonly IGrinder _grinder;
        private readonly IJuicerQualitySensor _sensor;
        private readonly IJuiceContainer _container;

        public Marc1000OrangePartsJuicer()
        {
            _sensor = new JuicerQualitySensor30();
            _container = new JuiceContainer20Liter();
            _grinder = new JuicerGrinder1000(_sensor, _container);
        }

        public override IGrinder GetGrinder()
        {
            return _grinder;
        }

        public override IJuicerQualitySensor GetSensor()
        {
            return _sensor;
        }

        public override IJuiceContainer GetJuicerContainer()
        {
            return _container;
        }
    }
}